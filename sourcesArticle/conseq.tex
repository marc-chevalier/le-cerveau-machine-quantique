\subsection{Décohérence dans les neurones}

Nous allons nous intéresser aux temps caractéristiques de décohérence des ions \ce{Na+}, causés respectivement par des collisions avec d'autres ions, des molécules 
d'eau, et l'interaction avec des ions plus éloignés \cite{tegmark}. On pourrait arguer que d'autres phénomènes pourraient être susceptibles d'avoir une influence plus grande et 
donc de causer la décohérence des ions \ce{Na+} plus rapidement ; mais les phénomènes que nous allons étudier nous donnent simplement une borne supérieure sur ces 
temps de décohérence.\\

Le système étudié sera donc un ion \ce{Na+}, de matrice de densité $\rho$. L'évolution de $\rho$ est donnée par l'équation :
\begin{equation}
  \rho(x,x',t_{0}+t) = \rho(x,x',t_{0})f(x,x',t)
\end{equation}
où $f$ est une certaine fonction, indépendante de l'état de l'ion et donc de $\rho$, qui ne dépend que de $H_{int}$ le hamiltonien d'interaction.

\subsubsection{Collisions entre ions}

Ici, $\Delta x$ est de l'ordre de l'épaisseur de la membrane, soit environ $\SI{10}{nm}$. La longueur d'onde de \textsc{De Broglie} d'un ion \ce{Na+} (de masse 
$m \approx 23m_{p}$) est :
\begin{equation}
  \lambda = \frac{2\pi\hbar}{\sqrt{3mk_{B}T}} \approx \SI{0,03}{nm}
\end{equation}
en prenant $T = \SI{37}{\degC} \approx \SI{310}{K}$ (température du corps humain). Nous nous trouvons donc clairement dans le cas de l'approximation $\Delta x \gg \lambda$, qui correspond au cas où la décohérence s'installe le plus rapidement ;  
nous avons donc $f$ de la forme $f(x,x',t) = e^{-\Gamma_{tot} t}$ avec $\Gamma_{tot}$ la constante de dispersion. Les ions \ce{Na+} décohèrent donc en un temps de l'ordre de 
$\Gamma_{tot}^{-1}$. Étant donné que l'état du neurone (\og au repos \fg{} ou \og activé \fg{}) de $N$ tels ions, le temps caractéristique 
de décohérence pour un neurone sera de $\tau = (N\Gamma_{tot})^{-1}$. $\Gamma_{tot}$ est donné par $\Gamma_{tot} = n \langle \sigma v \rangle$, où $n$ est la densité en ions, $\sigma$ 
la section efficace de la dispersion et $v$ la vitesse des ions. On évalue chacun de ces différents paramètres :
\begin{equation}
  n = \nu n_{H_{2}O} \sim 10^{23}\times 10^{-4} = \SI{10^{19}}{cm^{-3}}
\end{equation}
où $\nu$ est la concentration relative en ions (\ce{K+} et \ce{Na+}) et $n_{H_{2}O}$ la densité des molécules d'eau ;
\begin{equation}
  \sigma \sim \left(\frac{gq^{2}}{mv^{2}}\right)^{2} 
\end{equation}
où $g = \frac{1}{4 \pi \epsilon_{0}}$ est la constante de Coulomb, et $q$ la charge des ions ;
\begin{equation}
  v \sim \sqrt{\frac{k_{B}T}{m}}
\end{equation}
en considérant que les molécules sont à l'équilibre thermique et ont donc une énergie cinétique de $k_{B}T$ (à un facteur $1/2$ près).\\
On peut donc finalement évaluer $\tau$ : 
\begin{equation}
\tau \sim \frac{1}{N n\sigma v} \sim \frac{\sqrt{m(k_{B}T)^{3}}}{Ng^{2}q^{4}n} \sim \SI{10^{-20}}{s}
\end{equation}

\subsubsection{Collisions ions-eau}

Le raisonnement est passablement similaire à celui concernant les collisions entre ions ; la longueur d'onde de \textsc{De Broglie} d'une molécule d'eau est 
similaire à celle d'un ion \ce{Na+} on se trouve donc toujours dans le cas de l'approximation $\Delta x \gg \lambda$. $\sigma$ change : on a désormais 
\begin{equation}
  \sigma \sim \frac{gqp}{mv^{2}} 
\end{equation}
où $p \approx \SI{1,85}{D}$ est le moment dipolaire de l'eau.\\
D'où :
\begin{equation}
\tau \sim \frac{1}{N n\sigma v} \sim \frac{\sqrt{mk_{B}T}}{Ngqpn} \sim \SI{10^{-20}}{s}
\end{equation}

\subsubsection{Interaction avec des ions éloignés}

Pour ce genre d'interactions, on a 
\begin{equation}
  f(x,x',t) = \hat{p_{2}}\left(\frac{M(r-r')t}{\hbar}\right)
\end{equation}
où $\hat{p_{2}}$ est la transformée de Fourier de $p_{2}(r) = \rho_{2}(r,r)$, la distribution de probabilité de la localisation de l'ion éloigné, et M est la matrice 
des dérivées partielles secondes du potentiel d'interaction entre les deux particules. On peut ici approximer $\rho_{2}$ par une gaussienne :
\begin{equation}
f(r,r,t)=\exp\left(-\frac{(r'-r)^{t}\!M^{t}\!\Sigma M(r'-r)t^{2}}{2\hbar^{2}}\right)
\end{equation}
où $\Sigma$ est la matrice de variance-covariance de la position de l'ion éloigné. Le temps caractéristique de décohérence est donc :
\begin{equation}
  \tau = \frac{\hbar}{\sqrt{(r'-r)^{t}\!M^{t}\!\Sigma M(r'-r)}}
\end{equation}
En prenant $V = \frac{gq^{2}}{r'-r}$, on a : 
$M = \frac{2gq^{2}}{(r'-r)^{3}} \begin{pmatrix} 1 & 1 \\ 1 & 1 \end{pmatrix}$ ; en outre, à l'équilibre thermique, $\Sigma = (\Delta x)^{2} I$ ; ce qui nous donne :
\begin{equation}
  \tau = \frac{\hbar a^{3}}{gq^{2}|r'-r|\Delta x}
\end{equation}
où $a$ est la distance entre les deux ions.
On prend $\Delta x$ aussi petit que nous l'autorise le principe d'incertitude. Étant à l'équilibre thermique, on a $\frac{(\Delta p)^{2}}{m} \leqslant k_{B}T$, on a 
donc 
\begin{equation}
  \Delta x = \frac{\hbar}{2\Delta p} \sim \frac{\hbar}{\sqrt{mk_{B}T}}
\end{equation}
En réinjectant dans l'équation précédente, cela nous donne : 
\begin{equation}\label{decoherenceGen1}
  \tau \sim \frac{a^{3}\sqrt{mk_{B}T}}{Ngq^{2}|r'-r|}
\end{equation}
où N est le nombre d'ions exerçant une telle influence. En considérant que $a \sim n^{-1/3}$ et en s'intéressant à des ions les plus proches possibles ($|r'-r|=h$), 
on obtient 
\begin{equation}
  \tau \sim \frac{\sqrt{mk_{B}T}}{Ngq^{2}nh} \sim \SI{10^{-19}}{s}
\end{equation}

\subsection{Décohérence dans les microtubules}

On va maintenant donner un ordre de grandeur du temps de décohérence en ce qui concerne le mécanisme de propagation d'information dans les microtubules.

On note par $x$ la coordonnée le long de l'axe du microtubule. On modélise l'état du microtubule par une fonction $p : x \mapsto p(x)$ où $p(x)$ représente la composante du moment dipolaire électrique selon l'axe du microtubule. La charge par unité de longueur est donc $-p'(x)$. 

Avec le modèle de propagation exposé, $p(x)$ est de la forme :

\begin{equation}
p(x) = \begin{cases} 
+p_0 & \text{si } x\ll x_0 \\ 
-p_0 & \text{si } x\gg x_0
\end{cases}
\end{equation}

où $x_0$ représente l'endroit de la perturbation qui se propage à vitesse constante. En outre, la charge totale autour de la perturbation est $\int -p'(x) dx = 2 p_0 \sim 940 q_e$, qui est la charge due à la contribution des 13 protofilaments. $p_0$ est donc tel qu'il vérifie cette condition.

On va maintenant supposer l'existence d'une superposition d'états et examiner le temps nécessaire pour que cette superposition soit perdue par décohérence. On suppose la superposition constituée de deux états de propagation où les perturbations sont distantes de $\vert r - r' \vert$.

On néglige les interactions avec les molécules d'eau voisines. En effet, elles peuvent faire partie du système quantique mais ce phénomène est difficile à quantifier. Nous ne considérerons que les interactions avec les ions distants, ce qui est déjà suffisant pour avoir une borne supérieure. On peut donc appliquer directement l'équation (\ref{decoherenceGen1}) avec $N=Q/q_e \sim 10^3$. La distance à l'ion le plus proche est généralement inférieure à $a=D+n^{-1/3} \sim \SI{26}{nm}$. On remarque que $D+n^{1/3}$ peut être approximé à $D$ qui vaut \SI{24}{nm}. Aussi, on approximera $a^3$ par $D^3$. On suppose aussi que la superposition s'étend sur une longueur bien supérieure à la taille d'un dimère de tubuline, ce qui implique $\vert r'-r \vert \ll D$. Avec ces remarques, le temps de décohérence dû seulement à l'ion le plus proche sera alors de l'ordre de :

\begin{equation}
\tau \sim \frac{D^2 \sqrt{mkT}}{Ngq_e^2} \sim \SI{10^{-13}}{s}
\end{equation}
\pagebreak

\subsection{Synthèse sur les ordres de grandeurs}

Comparons à présent les temps de décohérence et les temps biologiques (c'est à dire les temps d'évolution et de transfert de l'information biologique).
\vspace{\baselineskip}

Le tableau \ref{tab2} regroupe les résultats obtenus pour les \textbf{temps de décohérence} de différents systèmes en interaction avec différents environnements (voir les paragraphes précédents).

\begin{table}[!h]
\begin{center}
	\begin{tabular}{ccc}
		\hline
		\hline
		 \textit{Système} & \textit{Environnement} & \textit{Temps de décohérence} \\
		\hline
		 Neurone & \textbf{Collision} de \ce{Na+} avec d'autres \ce{Na+} et des \ce{K+} & $10^{-20}$ \\
		Neurone & \textbf{Collision} de \ce{Na+} avec des molécules d'eau & $10^{-20}$ \\
		Neurone & \textbf{Interaction} de \ce{Na+} avec d'autres ions à distance & $10^{-19}$ \\
		Microtubule & \textbf{Interaction} de \ce{Na+} avec d'autres ions à distance & $10^{-13}$ \\
		\hline
		\hline
	\end{tabular}
	\caption{\label{tab2} Temps de décohérence (en secondes) pour des neurones et microtubules en interaction avec leur environnement}
\end{center}
\end{table}
\FloatBarrier

En ce qui concerne les \textbf{temps biologiques}, la durée caractéristique des mécanismes de la transmission d'un signal dans un neurone est estimée à $\SI{10^{-3}}{s}$ ; quant au temps que met la polarisation pour changer de sens dans une microtubule, il est du même ordre de grandeur.
\vspace{\baselineskip}

Nous remarquons tout de suite que les temps de décohérence sont bien plus faibles que les temps biologiques, d'un facteur $10^{-10}$ pour les temps les plus proches. Les temps de fonctionnement biologiques sont donc incompatibles avec l'hypothèse d'un fonctionnement quantique du cerveau. En effet, ils sont bien trop grands ; et le système aura totalement décohéré avant que l'information quantique ne soit transmise via les processus biologiques.
\vspace{\baselineskip}

Quant aux temps caractéristiques de processus cognitifs (tels que les actions délibérées, les pensées, les réponses à des stimuli externes), ils sont de l'ordre de $10^{-2}$ à $\SI{1}{s}$. Ces temps sont là aussi bien supérieurs aux temps de décohérence. La mécanique quantique ne semble donc pas pouvoir avoir un rôle dans ces processus cognitifs et dans l'émergence de la conscience.
