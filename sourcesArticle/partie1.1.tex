\section{Introduction}

  À ses débuts, la mécanique quantique s'est bornée à l'étude de systèmes isolés, qui sont les plus simples à étudier. Lors d'expérimentations, l'hypothèse de non-interaction avec l'environnement n'est pas vérifiée à cause de nombreux phénomènes : le vide du laboratoire n'est pas parfait, des photons dus à l'éclairage, au rayonnement thermique des divers objets, et même au rayonnement thermique $\SI{3}{K}$ entrent en interaction avec le système étudié. Il est donc nécessaire de considérer la présence de l'environnement pour étudier un système quantique. Nous verrons que ce sont ces interactions avec l'environnement qui mènent à la décohérence.
  
  La décohérence est également très importante car elle explique pourquoi les effets de la mécanique quantique ne peut être observés à l'échelle du monde classique\footnote{Plus précisément, un objet macroscopique placé dans une superposition d'états décohère en un temps très court, et ne peut pas être observé dans cette superposition d'états.}, en particulier, la décohérence explique partiellement le processus de mesure, qui était jusqu'ici une violation apparente entre le postulat V\footnote{Postulat de réduction du paquet d'ondes} et le postulat VI\footnote{Équation de \textsc{Schrödinger}} de la mécanique quantique.
  
  Dans cette partie, nous développerons un exemple d'interaction entre un système et son environnement pour illustrer le phénomène de décohérence, et nous présenterons quelques ordres de grandeur de temps de décohérence \cite{Schlosshauer}.

\section{Qu'est-ce que la décohérence ?}

  La décohérence est l'évolution de l'état d'un système sous l'influence de son environnement pour passer d'un état pur à un état impur. Plus formellement, le matrice densité d'un état pur peut s'écrire comme la projection sur son état $\psi$ : $\ket{\psi(t=0)}\bra{\psi(t=0)}$. Après évolution sous l'action du hamiltonien dû à l'environnement, la matrice densité du système ne peut plus s'écrire $\ket{\psi(t)}\bra{\psi(t)}$, mais $\sum \ket{\phi}\bra{\phi}$. De plus, les termes hors diagonale de la matrice densité tendent vers $0$ lorsque le système évolue.
  
  La principale conséquence de cette forme pour la matrice densité est que le système ne peut plus interférer avec lui-même (par exemple dans une expérience de fentes d'\textsc{Young}).

\section{Exemple de la bille diffusée par un environnement de petites particules}

  Pour mieux comprendre le phénomène de décohérence, étudions maintenant une situation particulière de décohérence : une bille (le système étudié, noté $S$) diffusée par de petites particules (l'environnement considéré, noté $\varepsilon$) (voir figure \ref{fig:diffparts}). On supposera que la bille est beaucoup plus massive que les particules la frappant. On étudiera une seul collision pour commencer (voir figure \ref{fig:diff1part}).
  
\begin{figure}[!h]
  \begin{minipage}{0.45\textwidth}
    \centering
    \input{images/bille2.eps_tex}
    \caption{Bille dans son environnement diffusif}
    \label{fig:diffparts}
  \end{minipage}
  \hfill
  \begin{minipage}{0.45\textwidth}
    \centering
    \input{images/bille.eps_tex}
    \caption{Diffusion d'une seule particule sur la bille}
    \label{fig:diff1part}
  \end{minipage}
\end{figure}

  On note $\ket{\textbf{x}}$ l'état propre de l'opérateur position du système associé à la valeur propre $\textbf{x}$. L'état de la particule diffusant sur la bille est noté $\ket{\chi_i}$ avant le choc, et $\ket{\chi(\textbf{x})}$ après le choc. 

  On suppose que $S$ et $\varepsilon$ ne sont pas corrélés avant le choc. Donc avant le choc, la matrice densité de $S \cup \varepsilon$ peut s'écrire $\hat{\rho}(t=0)=\hat{\rho}_S(t=0)\otimes \hat{\rho}_\varepsilon(t=0)$.
  
  On note $\hat{S}$ l'opérateur qui transforme le système pendant le choc. Ainsi, un choc s'écrit : $\ket{\textbf{x}}\otimes \ket{\chi_i} \mapsto \hat{S}\ket{\textbf{x}}\otimes \ket{\chi_i}$.
  
  Enfin, on repère l'état du système à partir de l'origine du repère : $\ket{\textbf{x}}=e^{-i\hat{\textbf{p}}.\textbf{x}/\hbar}\ket{\textbf{x}=\textbf{0}}$.
  
  On peut alors écrire l'état du système après un choc :
\begin{equation}
  \hat{S}\ket{\textbf{x}}\ket{\chi_i}=\hat{S}e^{-i\hat{\textbf{p}}.\textbf{x}/\hbar}\ket{\textbf{0}}\ket{\chi_i}=\hat{S}e^{-i(\hat{\textbf{p}}+\hat{\textbf{q}}).\textbf{x}/\hbar}\ket{\textbf{0}}e^{i(\hat{\textbf{q}}).\textbf{x}/\hbar}\ket{\chi_i}
\end{equation}
  Où $\hat{\textbf{q}}$ est l'opérateur impulsion de l'environnement, qui n'agit donc pas sur l'état du système.

  Notons $\hat{\textbf{P}}=\hat{\textbf{p}}+\hat{\textbf{q}}$ l'opérateur impulsion du système total. $\hat{\textbf{P}}$ engendre les translations du système $S\cup\varepsilon$. Or les lois de la physique sont les mêmes dans tout l'espace, donc $[\hat{S},\hat{\textbf{P}}]=0$. On peut donc écrire :
 
\begin{equation}
  \ket{\textbf{x}}\ket{\chi_i}=\hat{S}e^{-i(\hat{\textbf{p}}+\hat{\textbf{q}}).\textbf{x}/\hbar}\hat{S}\ket{\textbf{0}}e^{i(\hat{\textbf{q}}).\textbf{x}/\hbar}\ket{\chi_i}
\end{equation}
  
  \pagebreak
  Comme on a fait l'hypothèse que la bille est beaucoup plus massive que les particules de l'environnement, la position de la bille n'est pas altérée par le choc : $\hat{S}\ket{\textbf{x}}\ket{\chi_i}=\ket{\textbf{x}}\hat{S_0}\ket{\chi_i}$. On en déduit :
  
\begin{equation}
  \begin{aligned}
\hat{S}\ket{\textbf{x}}\ket{\chi_i}
 &= e^{-i(\hat{\textbf{p}}+\hat{\textbf{q}}).\textbf{x}/\hbar}\ket{\textbf{0}}\hat{S_0}e^{i(\hat{\textbf{q}}).\textbf{x}/\hbar}\ket{\chi_i}\\
 &= e^{-i\hat{\textbf{p}}.\textbf{x}/\hbar}\ket{\textbf{0}}e^{-i\hat{\textbf{q}}.\textbf{x}/\hbar}\hat{S_0}e^{i\hat{\textbf{q}}.\textbf{x}/\hbar}\ket{\chi_i}\\
 &= \ket{\textbf{x}}e^{-i\hat{\textbf{q}}.\textbf{x}/\hbar}\hat{S_0}e^{i\hat{\textbf{q}}.\textbf{x}/\hbar}\ket{\chi_i}\\
 &=: \ket{\textbf{x}} \hat{S_{\textbf{x}}} \ket{\chi_i}
\end{aligned}
\end{equation}

  On définit ainsi $\hat{S_{\textbf{x}}}=e^{-i\hat{\textbf{q}}.\textbf{x}/\hbar}\hat{S_0}e^{i\hat{\textbf{q}}.\textbf{x}/\hbar}$, l'opérateur qui traduit le choc pour les petites particules : $\ket{\chi_{\textbf{x}}}= \hat{S_{\textbf{x}}} \ket{\chi_i}$.

  Du point de vue des matrices densité, après le choc, on a :
\begin{equation}
\hat{\rho}=\int d\textbf{x} \int d\textbf{x'} \rho_S(\textbf{x},\textbf{x'},0) \ket{\textbf{x}} \bra{\textbf{x'}} \otimes \ket{\chi(\textbf{x})} \bra{\chi(\textbf{x'})}
\end{equation}
  Donc la matrice densité réduite s'écrit :
  
 
\begin{equation}
\hat{\rho_S}=\int d\textbf{x} \int d\textbf{x'} \rho_S(\textbf{x},\textbf{x'},0) \ket{\textbf{x}} \bra{\textbf{x'}}  \braket{\chi(\textbf{x'}) | \chi(\textbf{x})}
\end{equation}

  Ainsi, un choc peut se résumer à $\rho_S(\textbf{x},\textbf{x'},0) \mapsto \rho_S(\textbf{x},\textbf{x'},0) \braket{\chi(\textbf{x'}) | \chi(\textbf{x})}$.

  Arrêtons-nous ici pour étudier deux cas particuliers : 
  
  \begin{itemize}
    \item Si $\ket{\chi(\textbf{x})}$ et $\ket{\chi(\textbf{x'})}$ sont orthogonaux, c'est-à-dire si on est capable de mesurer à coup sûr l'état de la particule diffusée après le choc, alors on connaît la position de la bille. Il y a alors décohérence totale. C'est ce qu'on retrouve par le calcul que nous venons d'effectuer car $\braket{\chi(\textbf{x'}) | \chi(\textbf{x})}=0$ : tous les termes hors diagonaux de la matrice densité réduite sont nuls.
    \item Au contraire, si $\braket{\chi(\textbf{x'}) | \chi(\textbf{x})}=1$, aucune mesure sur l'état de l'environnement ne peut permettre de déduire la position de la bille. La cohérence reste parfaite et la matrice densité réduite n'est changée que par des changements de phase.
  \end{itemize}
  
  On remarque que le recouvrement $\braket{\chi(\textbf{x'}) | \chi(\textbf{x})}$ est une grandeur très importante dans ce problème, on cherchera donc à l'estimer, et en particulier à étudier sa dépendance temporelle.
  
  \section{Le terme de recouvrement}
  
  D'après ce qu'on a vu sur l'écriture de $\ket{\chi(\textbf{x})}$, on peut écrire le terme de recouvrement de la façon suivante.
\begin{equation}
  \begin{aligned}
    \braket{\chi(\textbf{x'}) | \chi(\textbf{x})} &= \bra{\chi_i} \hat{S}^{\dagger}_{\textbf{x}} \hat{S}_{\textbf{x}} \ket{\chi_i}\\
    &= \text{Tr}_{\varepsilon} \{ \hat{\rho}_{\varepsilon}(0) \hat{S}^{\dagger}_{\textbf{x}} \hat{S}_{\textbf{x}} \}
  \end{aligned}
\end{equation}
  
où $A^\dagger$ dénote la matrice transconjuguée (ie. adjointe) de $A$ ($A^\dagger = A^* = {}^t\!\bar{A}$).
  
 \pagebreak  
  
  De plus, on décompose $\hat{\rho}_{\varepsilon}(0)$ sur la base propre de l'opérateur impulsion $\{ \ket{\textbf{q}} \}$ qu'on normalise en considérant que la petite particule ne peut se déplacer que dans une boite de volume $V$ ($\ket{\widetilde{\textbf{q}}} = \left(\frac{(2\pi \hbar)^3}{V}\right)^{1/2} \ket{\textbf{q}}$) : 
  
\begin{equation}
  \hat{\rho}_{\varepsilon}(0)=\frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) \ket{\widetilde{\textbf{q}}} \bra{\widetilde{\textbf{q}}}
\end{equation}

  Le facteur de recouvrement s'écrit alors : 
  
\begin{equation}
  \begin{aligned}
    \braket{\chi(\textbf{x'}) | \chi(\textbf{x})} &= \frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) \ket{\widetilde{\textbf{q}}} \hat{S}^{\dagger}_{\textbf{x}} \hat{S}_{\textbf{x}} \bra{\widetilde{\textbf{q}}}\\
    &= \frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) e^{i \textbf{q} (\textbf{x}-\textbf{x'})/\hbar} \bra{\widetilde{\textbf{q}}} \hat{S}^{\dagger}_{\textbf{0}} e^{-i \textbf{q} (\textbf{x}-\textbf{x'})/\hbar} \hat{S}_{\textbf{0}} \ket{\widetilde{\textbf{q}}}
  \end{aligned}
\end{equation}
  
  Pour continuer, il nous faut plus d'informations sur $\hat{S}_0$. La théorie de la diffusion donne comme résultat que celui-ci peut s'écrire $\hat{S}_0 = \hat{\mathbb{I}} + i\hat{T}$ avec 

\begin{equation}  
  \begin{aligned}
    \bra{\textbf{q}} \hat{T} \ket{\textbf{q'}} &= \frac{i}{2\pi \hbar q} \delta(q-q') f(\textbf{q},\textbf{q'})\\
    &= \frac{i}{2\pi \hbar m} \delta(E-E') f(\textbf{q},\textbf{q'})
  \end{aligned}
\end{equation}
  
  avec $q=\|\textbf{q}\|$.
  
  Le $\delta(E-E')$ reflète le fait qu'il n'y a pas d'énergie de recul et donc que l'énergie du système $S$ ne change pas pendant le choc.
  
  On peut alors poursuivre le calcul :
  
  \begin{equation}
  \begin{aligned}
    \braket{\chi(\textbf{x'}) | \chi(\textbf{x})} &= \frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) e^{i \textbf{q} (\textbf{x}-\textbf{x'})/\hbar} \bra{\widetilde{\textbf{q}}} (\hat{\mathbb{I}} - i\hat{T}^\dagger) e^{-i \textbf{q} (\textbf{x}-\textbf{x'})/\hbar} \hat{S}_{\textbf{0}} \ket{\widetilde{\textbf{q}}}\\
    &= \frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) \left[ 1 - \braket{\widetilde{\textbf{q}}| \hat{T} \hat{T}^\dagger \widetilde{\textbf{q}}} + e^{i\textbf{q}.(\textbf{x}-\textbf{x'})/\hbar} \braket{\widetilde{\textbf{q}} | \hat{T}^\dagger e^{-i\hat{\textbf{q}}.(\textbf{x}-\textbf{x'})/\hbar} \hat{T} | \widetilde{\textbf{q}} } \right]\\
    &= \frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \mu(\textbf{q}) \left[1 - \sum_{\textbf{q'}} \left(1 - e^{i(\textbf{q}-\textbf{q'}).(\textbf{x}-\textbf{x'})/\hbar} \right) | \bra{\widetilde{\textbf{q}}} \hat{T} \ket{\widetilde{\textbf{q'}}} |^2 \right]
  \end{aligned}
  \end{equation}
  
  On passe maintenant à la limite continue $\frac{(2\pi \hbar)^3}{V} \sum_{\textbf{q}} \to \int d\textbf{q}$. On rappelle que\\ $\int d\textbf{q} \mu(\textbf{q}) = 1$.
  
\begin{equation}  
  \braket{\chi(\textbf{x'}) | \chi(\textbf{x})} = 1 - \int d\textbf{q} \mu(\textbf{q}) \frac{(2\pi \hbar)^3}{V} \int d\textbf{q'} \left(1 - e^{i(\textbf{q}-\textbf{q'}).(\textbf{x}-\textbf{x'})/\hbar} \right) | \bra{\textbf{q}} \hat{T} \ket{\textbf{q'}} |^2
\end{equation}

\pagebreak
  
  Dans le terme $| \bra{\textbf{q}} \hat{T} \ket{\textbf{q'}} |^2$ intervient un $\delta^2(E-E')$, que l'on réécrit :
 
\begin{equation}
  \begin{aligned}
    \delta^2(E-E') &= \delta(E-E').\delta(E-E')\\
    &= \delta(E-E').\lim\limits_{T \to \infty} \frac{1}{2\pi \hbar} \int_{-T/2}^{T/2} dt\ e^{i(E-E')t/\hbar}\\
    &= \delta(E-E').\lim\limits_{T \to \infty} \frac{1}{2\pi \hbar} \int_{-T/2}^{T/2} dt\\
    &= \delta(E-E').\lim\limits_{T \to \infty} \frac{T}{2\pi \hbar}
  \end{aligned}
\end{equation}
  
  Or ce que l'on a écrit n'est valable que pendant la durée de l'interaction entre la bille et la particule. Or le choc entre les deux particules est court. Donc $\delta^2(E-E') = \delta(E-E') \frac{T}{2\pi \hbar} = \delta(\textbf{q}-\textbf{q'}) \frac{m}{q} \frac{T}{2\pi \hbar}$.
  
  \section{Le facteur de décohérence}
  
  Rappelons l'évolution de la matrice densité :
  
\begin{equation}
 \rho_S(\textbf{x},\textbf{x'},0) \mapsto \rho(\textbf{x},\textbf{x'},0) \braket{\chi(\textbf{x'}) | \chi(\textbf{x})}
\end{equation}
\begin{equation}  
  \text{et } \braket{\chi(\textbf{x'}) | \chi(\textbf{x})} = 1 - \int d\textbf{q} \mu(\textbf{q}) \frac{(2\pi \hbar)^3}{V} \int d\textbf{q'} \left(1 - e^{i(\textbf{q}-\textbf{q'}).(\textbf{x}-\textbf{x'})/\hbar} \right) | \bra{\textbf{q}} \hat{T} \ket{\textbf{q'}} |^2
\end{equation}
  
  On peut donc écrire, en notant $d\textbf{q'} = q d\hat{n'}$ et en remarquant que le $\delta$ est nul quand $\|\textbf{q}\| \neq \|\textbf{q'}\|$ : 
  
\begin{equation}
  \begin{aligned}
    \rho_S(\textbf{x},\textbf{x'},T) - \rho_S(\textbf{x},\textbf{x'},0) &= - \rho_S(\textbf{x},\textbf{x'},0) \int d\textbf{q} \mu(\textbf{q}) \frac{(2\pi \hbar)^3}{V} \int d\textbf{q'} \left(1- e^{i(\textbf{q}-\textbf{q'}).(\textbf{x}-\textbf{x'})/\hbar}\right) | \bra{\textbf{q}} \hat{T} \ket{\textbf{q'}} |^2\\
    &= - \rho_S(\textbf{x},\textbf{x'},0) \frac{T}{V} \int d\textbf{q} \mu(\textbf{q}) v(q) \int d\hat{n'} \left(1 - e^{i(\textbf{q}-q\hat{n'}).(\textbf{x}-\textbf{x'})/\hbar}\right) |f(\textbf{q},q\hat{n'})|^2
  \end{aligned}
\end{equation}
  
  En faisant tendre $T$ vers $0$, car la durée du choc est très courte, et en posant $\mu(\textbf{q}) = \frac{1}{4\pi} \frac{V}{N} \rho(q)\ dq\ d\hat{n}$, on peut écrire cette égalité sous forme de dérivée.
  
  
\begin{equation}  
  \frac{\partial \rho_S(\textbf{x},\textbf{x'},t)}{\partial t} = - F(\textbf{x}-\textbf{x'}) \rho_S(\textbf{x},\textbf{x'},t)
\end{equation}
  
\begin{equation}
  \text{avec } F(\textbf{x}-\textbf{x'}) = \int dq\ \rho(q)v(q) \int \frac{d\hat{n}\ d\hat{n'}}{4\pi} \left(1 - e^{iq(\hat{n}-\hat{n'}).(\textbf{x}-\textbf{x'})/\hbar}\right) | f(q\hat{n},q\hat{n'}) |^2
\end{equation}
 
  Où $F(\textbf{x}-\textbf{x'})$ est appelé facteur de décohérence. Nous chercherons maintenant à évaluer ce facteur, afin de déterminer comment évolue la matrice densité. On peut d'ores et déjà observer que ses termes diagonaux, qui représentent les probabilités d'observer le système dans les différents états, n'évoluent pas avec le temps car $F(0) = 0$. Les termes hors diagonale, qui représentent les interférences du système avec lui-même, sont les seuls à être affectés par le phénomène de décohérence.
  
  
  
  
  
