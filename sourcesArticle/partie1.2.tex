\section{Évolution de la matrice densité - Système en interaction avec l'environnement}

Nous allons à présent nous intéresser à l'évolution de la matrice de densité du système.
Nous considérerons, dans toute la suite, un objet décrit par la superposition cohérente de deux paquets d'onde bien localisés, espacés de $\Delta x = |x-x'|$.
La question que l'on se pose est de savoir à quelle vitesse cette superposition cohérente va, en présence de l'environnement, se décohérer, c'est à dire à quelle vitesse les deux paquets d'ondes vont être résolus (c'est à dire différenciés l'un de l'autre).

Nous pouvons distinguer deux cas pour étudier cette évolution. Le cas où l'objet est \og grand \fg{} par rapport à la longueur d'onde de l'environnement ($\Delta x \gg \lambda_0$), et celui où il est \og petit \fg{} ($\Delta x \ll \lambda_0$). \og Grand \fg{} et \og petit \fg{} ne sont pas tout à fait les mots appropriés, mais nous les emploierons pour se faire une idée.
\vspace{\baselineskip}

Dans le cas \textbf{$\Delta x \gg \lambda_0$} (objet \og grand \fg{}), la décohérence s'installe très rapidement. En effet, la longueur d'onde est suffisamment petite pour que l'objet puisse \og sentir \fg{} son passage. Les paquets d'ondes seront alors fortement résolus, beaucoup d'informations sur l'objet seront alors dispersées dans l'environnement, provoquant une rapide et forte décohérence.

Les termes d'interférence de la matrice de densité du système sont ceux hors diagonale : $\rho_S(x,x',t)$ tel que $x \neq x'$. L'évolution de ces termes d'interférence se fait de la manière suivante :
\begin{equation}
	\frac{\partial \rho_S(x,x',t)}{\partial t} = -\Gamma_{tot}.\rho_S(x,x',t)
	\label{eq1}
\end{equation}

avec $\Gamma_{tot}$ le taux de décohérence maximum (ou constante de dispersion maximale), auquel est égal le facteur de décohérence $F(x-x')$ dans ce cas.
\vspace{\baselineskip}

Ce qui nous donne :
\begin{equation}
	\rho_S(x,x',t) = \rho_S(x,x',0).e^{-\Gamma_{tot}.t}
	\label{eq2}
\end{equation}

Cela montre l'existence d'un maximum de décohérence. Au bout d'un certain taux de décohérence, il ne peut y en avoir plus : les deux paquets d'onde sont en effet parfaitement résolus et ne peuvent l'être davantage. 
\vspace{\baselineskip}

Dans le cas limite opposé \textbf{$\Delta x \ll \lambda_0$} (objet \og petit \fg{}), l'objet ne va pas \og voir \fg{} l'environnement de longueur d'onde $\lambda_0$. La décohérence sera donc beaucoup plus longue à s'installer, et il faudra de nombreux événements dispersants pour introduire une grande localisation spatiale de l'objet (pour que ses deux paquets d'onde soient résolus).
\vspace{\baselineskip}

L'évolution des termes d'interférence (tels que $x \neq x'$) dépend cette fois de $\Delta x$ :

\begin{equation}
	\frac{\partial \rho_S(x,x',t)}{\partial t} = -\Lambda.(x-x')^2.\rho_S(x,x',t)
	\label{eq3}
\end{equation}

avec $\Lambda$ la \textbf{constante de dispersion}, qui contient les détails physiques de l'interaction.

Son expression est une intégrale sur l'ensemble des quantités de mouvement possibles, faisant intervenir la densité des particules de l'environnement ayant une quantité de mouvement $q$ donnée, leur vitesse, leur section efficace de collision, et la quantité de mouvement $q$ au carré.
\vspace{\baselineskip}

Nous pouvons tout de suite remarquer que pour $x = x'$, $\frac{\partial \rho_S(x,x',t)}{\partial t} = 0$, ce qui est bien en accord avec le fait que les termes de la diagonale, qui représentent les probabilités de trouver l'objet dans un état donné, ne sont pas modifiés par l'interaction avec l'environnement.
\vspace{\baselineskip}

Cette constante représente le taux de suppression de la cohérence.
\begin{equation}
	\rho_S(x,x',t) = \rho_S(x,x',0).e^{-\Lambda (\Delta x)^2.t}
	\label{eq4}
\end{equation}

Nous pouvons introduire le \textbf{temps de décohérence} $\tau_{\Delta x}$, donné par :
\begin{equation}
	\tau_{\Delta x} = \frac{1}{\Lambda (\Delta x)^2}
	\label{tau}
\end{equation}

Les événements dispersifs tendent à supprimer les termes hors diagonale. Cette suppression augmente exponentiellement avec $(\Delta x)^2$ et $t$.
Plus la largeur $\Delta x$ entre les deux paquets d'onde augmente - ce qui peut se comprendre par une augmentation de la taille de l'objet considéré - plus le temps de décohérence diminue. Le système devient rapidement décohérent (perte des interférences), et perd alors son caractère quantique.
\vspace{\baselineskip}

La figure \ref{evolmatricedensite} montre l'évolution de la matrice de densité en fonction de $\Delta x$ à t fixé. La figure \ref{evolmatricedensite_2} montre les allures de la courbe de la figure \ref{evolmatricedensite} pour différents temps.

\input{sourcesArticle/courbe1.tex}

\input{sourcesArticle/courbe2.tex}
\FloatBarrier

On remarque figure \ref{evolmatricedensite_2} que plus $|x-x'|$ est grand, plus le maximum de décohérence est atteint rapidement.
\vspace{\baselineskip}

La décohérence maximale est atteinte lorsque nous arrivons à la limite classique, c'est à dire lorsque tous les termes non diagonaux sont nuls. Nous verrons une représentation de l'établissement de la décohérence et des conséquences sur la matrice de densité du système dans le paragraphe suivant. 


\section{Évolution de la matrice densité - Système en mouvement et en interaction avec l'environnement}

Nous allons à présent nous pencher sur la dynamique de la décohérence.

Considérons une particule libre. Le hamiltonien qui décrit son évolution unitaire est le suivant :
\begin{equation}
	H(x,p) = - \frac{1}{2m} \frac{\partial^2}{\partial x^2}
	\label{hamiltonien}
\end{equation}

L'évolution de la matrice densité du système, lorsque nous considérons également la dynamique de l'objet {particule libre}, est donné par :

\begin{equation}
	\frac{\partial \rho_S(x,x',t)}{\partial t} = -\frac{i}{2m} \left(\frac{\partial^2}{\partial x'^2} - \frac{\partial^2}{\partial x^2}\right)\rho_S(x,x',t) -\Lambda.(x-x')^2.\rho_S(x,x',t)
	\label{eq5}
\end{equation}

Le premier terme du membre de droite correspond à la prise en compte de la dynamique du système, et le second à l'évolution due à l'interaction avec l'environnement, déjà étudiée dans la partie précédente.

Les solutions sont de la forme de la gaussienne suivante :
\begin{equation}
	\rho_S(x,x',t) = exp[-A(t)(x-x')^2 - i B(t)(x-x')(x+x') - C(t)(x+x')^2 - D(t)]
	\label{soleq5}
\end{equation}
où A(t), B(t), C(t) et D(t) sont des coefficients qui dépendent du temps et des conditions initiales.
\vspace{\baselineskip}

La figure \ref{matdensitegauss} montre une représentation de la description gaussienne de la matrice de densité du système.
\begin{figure}[H]
	\begin{center}
	\includegraphics[scale=0.4]{images/gaussienne.eps}
	\caption{Représentation de la description gaussienne de la matrice de densité du système}
	\label{matdensitegauss}
	\end{center}
\end{figure}

Nous remarquons dans l'équation \ref{soleq5} que pour $x=x'$, c'est à dire sur la diagonale de la matrice, les termes dans l'exponentielle qui dépendent de $(x-x')$ ne jouent plus aucun rôle. De même, pour $x=-x'$, ceux qui dépendent de $(x+x')$ n'interviennent plus.

La cohérence spatiale est définie par les termes hors diagonale. La largeur de la gaussienne sur la droite $x=-x'$ est liée au taux de décohérence. Nous remarquons que sa largeur parallèlement à cette droite est inversement proportionnelle à $\sqrt{A(t)}$. La \textbf{longueur de cohérence} est définie comme suit :
\begin{equation}
l(t) \equiv \frac{1}{\sqrt{8A(t)}}
\label{longcoh}
\end{equation}

La probabilité de trouver le système dans un état donné est, elle, liée aux termes de la diagonale $x=x'$. De même que précédemment, la largeur de la gaussienne parallèlement à la droite est inversement proportionnelle à $\sqrt{C(t)}$. On définit la \textbf{largeur d'ensemble} par : 
\begin{equation}
\Delta X(t) \equiv \frac{1}{\sqrt{8C(t)}}
\label{largens}
\end{equation}
 
Le terme $B(t)$ est lui lié à l'étalement de la distribution de quantité de mouvement. Nous nous y intéresserons moins dans la suite.
\vspace{\baselineskip}

Nous pouvons nous intéresser à l'évolution de la matrice densité du système, en regardant l'évolution de ces deux quantités caractéristiques que sont la longueur de cohérence et la largeur d'ensemble.

Pour cela, prenons un état initial simple : une distribution gaussienne centrée en 0.
\`{A} quelle évolution nous attendons-nous ?

Sans interactions avec l'environnement, longueur de cohérence et largeur de cohérence s'étaleraient chacune dans leur direction respective au cours du temps.
Ici, le système objet {particule libre} est en interaction avec l'environnement. De la décohérence va alors s'installer peu à peu (avec une vitesse dépendant du temps de décohérence). La longueur de cohérence va donc diminuer avec le temps, en compressant la gaussienne le long de l'axe $x=x'$. La limite est l'atteinte de la distribution classique, lorsque la distribution de la matrice de densité du système ne s'étendra plus que sur l'axe $x=x'$.
Quant à la largeur d'ensemble, elle s'étalera toujours dans la direction $x=x'$, mais plus rapidement que si il n'y avait pas d'interaction avec l'environnement. En effet, les événements dispersifs accroissent l'énergie du système (car celui-ci n'a pas d'énergie de recul, il encaisse donc l'énergie).%...... a compléter un peu avec réponse P.Degiovanni

%Avec $\psi(x,0)$ la fonction d'onde initiale (gaussienne), nous obtenons la matrice de densité initiale : 
%\begin{equation}
%\rho_S(x,x',0) = |\psi(x,0)><\psi(x,0)| = (\frac{1}{2\pi b^2})^{1/2} exp(-\frac{x^2+x'2}{4b^2})
%\label{matdensinit}
%\end{equation}
%avec $b$ la largeur de la fonction d'onde gaussienne.
\vspace{\baselineskip}

Les graphiques suivants (\ref{evollongcoh} et \ref{evollargens}) montrent l'évolution de la longueur de cohérence et de la largeur d'ensemble au cours du temps, avec et sans interaction avec l'environnement.
\begin{figure}[H]
\begin{minipage}[c]{.45\linewidth}
	\begin{center}
	\includegraphics[scale=0.4]{images/evolution_longcoherence.eps}
	\caption{Évolution de la longueur de cohérence au cours de temps}
	\label{evollongcoh}
	\end{center}
\end{minipage}
\hfill
\begin{minipage}[c]{.45\linewidth}
	\begin{center}
	\includegraphics[scale=0.4]{images/evolution_largensemble.eps}
	\caption{Évolution de la largeur d'ensemble au cours de temps}
	\label{evollargens}
	\end{center}
\end{minipage}
\end{figure}
Le temps est mesuré en unités de $\tau_b=\frac{1}{\Lambda b^2}$, le temps caractéristique de localisation (ou de perte de la cohérence), avec $b$ la largeur initiale du paquet d'onde. La longueur de cohérence et la largeur d'ensemble sont données en unité de $b$.
\vspace{\baselineskip}

Sans environnement, à partir de $t=\tau_b$, la fonction d'onde s'étale de la même manière dans la direction de la largeur d'ensemble et de la longueur de cohérence.

Avec la présence de l'environnement, on observe bien l'accélération de l'étalement de la largeur d'ensemble le long de la diagonale $x=x'$. Concernant la longueur de cohérence, elle décroit linéairement pour $t\ll\tau_b$%(taux de décroissance proportionnel à $\Lambda$)
, puis évolue en $1/\sqrt{t}$ pour $t\gg\tau_b$.% (avec une évolution qui ne dépend plus de $b$).
%Explication .... => cf P.Degiovanni
\vspace{\baselineskip}


L'évolution de la matrice densité peut être vue comme le résultat de la compétition entre les deux grandeurs vues ci-dessus (longueur de cohérence $l(t)$ et largeur d'ensemble $\Delta X(t)$).

Leur rapport est souvent considéré pour étudier leurs influences respective (voir \ref{ratio}).
\begin{equation}
	\delta(t) = \frac{l(t)}{\Delta X(t)}
	\label{ratio}
\end{equation}

La figure \ref{evolratio} montre l'évolution du rapport $\delta(t)$ au cours du temps (temps pris en unités de $\tau_b$).
\begin{figure}[H]
	\begin{center}
	\includegraphics[scale=0.4]{images/evolution_rapportlarglong.eps}
	\caption{Évolution du rapport $\delta(t)$ au cours du temps, avec ou sans l'influence de l'environnement}
	\label{evolratio}
	\end{center}
\end{figure}

Ce rapport permet de donner des informations sur la pureté de la matrice de densité. En effet, si la matrice est pure (fonction d'onde cohérente), le rapport est égal à 1, et sinon ce dernier décroît.

Sans environnement (matrice pure), $\delta(t)$ est constant égal à 1, nous retrouvons donc bien le fait que l'étalement dans les deux directions $x=x'$ et $x=-x'$ se fait de la même manière.

Avec environnement (matrice non pure), $\delta(t)$ décroit de plus en plus rapidement, ce qui montre que la longueur de cohérence $l(t)$ décroit plus rapidement que la largeur d'ensemble $\Delta X(t)$ ne croit.


\section{Exemples et ordres de grandeur}

Penchons-nous à présent sur des exemples concrets d'environnements.

La première étape, lorsque nos considérons un environnement donné, est d'estimer la constante de dispersion $\Lambda$. Cette constante caractérise l'environnement et le système, c'est elle qui contient les informations physiques les concernant. De là, nous pourrons déduire le temps de décohérence (grâce à l'équation \ref{tau}) pour différents systèmes (en fonction de $\Delta x$).
Pour obtenir la constante de dispersion (\ref{Lambda}), il nous faut connaître la densité $\rho(q)$ en particules de l'environnement ayant une quantité de mouvement $q$, leur vitesse $\vartheta(q)$, ainsi qu'estimer la section efficace de collision $\sigma_{eff}(q)$ (estimation faite en considérant soit la diffusion \textsc{Rayleigh}, soit la diffusion \textsc{Thompson} en fonction de l'environnement considéré).
\begin{equation}
	\Lambda \equiv \int dq \rho(q) \vartheta(q) \frac{q^2}{\hbar^2} \sigma_{eff}(q)
	\label{Lambda}
\end{equation}
$\Lambda$ s'exprime en $cm^{-2}.s^{-1}$.
\vspace{\baselineskip}

Le tableau \ref{tab1} regroupe les résultats obtenus pour différents types d'environnement, et pour deux types d'objet (des grains de poussière de taille $a=10^{-3}cm$ et des grosses molécules de taille $a=10^{-6}cm$).

\begin{table}[H]
\begin{center}
\caption{\label{tab1} Constante de dispersion $\Lambda$ et temps de décohérence $\tau_{\Delta x}$ pour deux systèmes et différents types d'environnement}
	\begin{tabular}{|c||c|c||c|c|}
		\hline
		 & \multicolumn{2}{c||}{\textit{Grain de poussière}} & \multicolumn{2}{c|}{\textit{Grosse molécule}} \\
		\hline
		\textit{Environnement} & $\Lambda$ en $cm^{-2}.s^{-1}$ & $\tau_{\Delta x}$ en $s$ & $\Lambda$ en $cm^{-2}.s^{-1}$  & $\tau_{\Delta x}$ en $s$\\
		\hline
		Photons à $3K$ & $10^{6}$ & $1$ & $10^{-12}$ & $10^{24}$\\
		Photons à $300K$ & $10^{24}$ & $10^{-18}$ & $10^{6}$ & $10^{6}$\\
		Vide de laboratoire & $10^{20}$ & $10^{-14}$ & $10^{14}$ & $10^{-2}$ \\
		Molécule $O_2$ à $300K$ & $10^{37}$ & $10^{-23}$ & $10^{31}$ & $10^{-11}$\\
		\hline
	\end{tabular}
\end{center}
\end{table}

Pour calculer le temps de décohérence, on utilise \ref{tau}. Dans les 3 premiers cas, on considère que la distance entre les paquets d'onde est $\Delta x = a$, avec $a$ la taille de l'objet considéré.
Dans le cas des molécules d'air, on prend $\Delta x = \lambda_B$, avec $\lambda_B$ la longueur d'onde thermique de De Broglie ($\lambda_B \approx 10 \angstrom$ à $\SI{300}{K}$), car pour un gaz de molécules \og semi-classique \fg{}, la longueur de cohérence est de l'ordre de grandeur de $\lambda_B$.
\vspace{\baselineskip}

Plus la constante de dispersion augmente, plus le temps de décohérence diminue, c'est à dire plus les états d'interférence disparaissent rapidement. 
Nous remarquons que, même dans le meilleur vide qui peut être créé en laboratoire, la constante de dispersion reste très élevée par rapport au cas où il y a le moins de décohérence possible (c'est à dire dans le fond cosmologique (photons à \SI{3}{K})). Cela s'explique par les quelques molécules qui restent dans le vide de laboratoire, et donc par l'existence de collisions, qui provoquent la décohérence du système beaucoup plus rapide.

Un résultat étonnant est celui concernant le temps de décohérence des photons à \SI{3}{K} et à \SI{300}{K}. Les valeurs entre les grains de poussière et les grosses molécules diffèrent d'un facteur $10^{24}$, ce qui montre la grande influence de la taille des objets considérés. Ce qui est étonnant est la valeur de $10^{24} s$ pour le temps de décohérence dans un environnement de photons à \SI{3}{K}. Cela veut donc dire que des systèmes sont toujours cohérents depuis la formation de l'univers (créé il y a \og seulement \fg{} $10^{17}$ secondes).

Les conditions qui nous intéressent le plus dans le cadre de ce rapport sont celles concernant les molécules d'air à température ambiante. Nous pouvons remarquer un facteur $10^{12}$ entre les grosses molécules et les grains de poussière $1000$ fois plus gros. Cela montre toujours la grande influence de la taille des objets considérés, mais nous remarquons également que plus on augmente la densité en particules de l'environnement, moins (cela est relatif) la taille a de l'influence (facteur $10^{24}$ pour les photons à \SI{3}{K} ou \SI{300}{K}, pour \og seulement \fg{} $10^{12}$ pour les molécules d'air).
\vspace{\baselineskip}

Nous comparerons dans la partie suivante les temps de décohérence avec les temps caractéristiques des systèmes biologiques, et verrons si l'hypothèse d'un cerveau quantique est cohérente avec ces valeurs.
